{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE OverloadedStrings #-}

import Control.Monad

import Options.Applicative
import qualified TestPatches
import qualified MakeConstraints

mode :: Parser (IO ())
mode = hsubparser $ mconcat
  [ command "test-patches" $ info testPatches (progDesc "build patched packages")
  , command "make-constraints" $ info makeConstraints (progDesc "generate a cabal.constraints file")
  ]
  where
    testPatches = TestPatches.testPatches <$> TestPatches.config
    makeConstraints =
      (MakeConstraints.makeConstraints >=> print)
      <$> argument str (metavar "DIR" <> help "patches directory")

main :: IO ()
main = do
  theMode <- execParser $ info (helper <*> mode) mempty
  theMode
